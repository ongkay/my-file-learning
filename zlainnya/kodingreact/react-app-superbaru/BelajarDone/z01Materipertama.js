// import Header from "./components/Header";

// function App() {
//   const title = "welcome to may App";
//   const age = 20;
//   const link = "https://youtube.com";

//   return (
//     <div>
//       <Header />
//       <h1>{title}</h1>
//       <h1>{age}</h1>
//       <a href={link}>Go my website</a>
//     </div>
//   );
// }

// export default App;

// //Cara Panggil Commponentnya cukup seperti ini saja :
// import Header from "./components/Header";

// function App() {

//   return (
//     <div>
//       <Header />
//     </div>
//   );
// }

// export default App;

// //Click Events tanpa parameter
// function App() {
//   const clickme = () => {
//     console.log("clicked");
//   };

//   return (
//     <div>
//       <button onClick={clickme}>click Here</button>
//     </div>
//   );
// }

// //Click Events dengan parameter
// function App() {
//   const clickme = (parameternya) => {
//     console.log("clicked " + parameternya);
//   };

//   return (
//     <div>
//       <button onClick={() => clickme("isi parameternya")}>click Here</button>
//     </div>
//   );
// }

// //--------------------------------------------------------------
// /* UseState Hook ==> Merubah secara live
//  * harus Import --> useState
//  * Misalnya ingin Merubah Title dan Age
//  */
// import { useState } from "react";

// function App() {
//   const [title, setTitle] = useState("sebelum diklik");
//   const [age, setAge] = useState(20);

//   const changeTitle = () => {
//     setTitle("sudah di Klik");
//     setAge(40);
//   };

//   return (
//     <div>
//       <h1>Judul: {title}</h1>
//       <h1>Usia:{age}</h1>
//       <button onClick={changeTitle}>Klik Change</button>
//     </div>
//   );
// }

// //--------------------------------------------------------------
// /**LOOPING LIST*/
// import { useState } from "react";

// function App() {
//   const [product, setProduct] = useState([
//     { id: 1, title: "produk 1", price: 150 },
//     { id: 2, title: "produk 2", price: 150 },
//     { id: 3, title: "produk 3", price: 350 },
//     { id: 4, title: "produk 4", price: 450 },
//     { id: 5, title: "produk 5", price: 550 },
//     { id: 6, title: "produk 6", price: 650 },
//   ]);

//   return (
//     <div>
//       <ul>
//         {product.map((product) => (
//           <li key={product.id}>
//             {product.title} - {product.price}
//           </li>
//         ))}
//       </ul>
//     </div>
//   );
// }

// //--------------------------------------------------------------

// /**PROPS
//  * contoh : cara menghapus suatu produk di browser
//  */
//  import { useState } from "react";

//  //ini adalah Komponentnya
//  const Productlist = ({ products, deleteProduct }) => {
//   return (
//     <div>
//       <ul>
//         {products.map((product) => (
//           <li key={product.id}>
//             {product.title} - {product.price}
//             <button onClick={() => deleteProduct(product.id)}>Delete</button>
//           </li>
//         ))}
//       </ul>
//     </div>
//   );
// };

// //Disini Render appnya
// function App() {
//   const [products, setProduct] = useState([
//     { id: 1, title: "produk 1", price: 150 },
//     { id: 2, title: "produk 2", price: 150 },
//     { id: 3, title: "produk 3", price: 350 },
//     { id: 4, title: "produk 4", price: 450 },
//     { id: 5, title: "produk 5", price: 550 },
//     { id: 6, title: "produk 6", price: 650 },
//   ]);

//   const deleteProduct = (produkId) => {
//     const newProducts = products.filter((product) => product.id !== produkId);
//     setProduct(newProducts);
//   };

//   return (
//     <div>
//       <Productlist products={products} deleteProduct={deleteProduct} />
//     </div>
//   );
// }

// //--------------------------------------------------------------

// /**UseEffect Hook
//  * wajib import useEffect from react
//  */
//  import { useState, useEffect } from "react";

// function App() {

//   const [name, setName] = useState('Nama Kamu')

//   useEffect(() => {
//     console.log("menggunakan useEffect saat klikbutton")
//   }, [name])

//   return (
//     <div>
//       <button onClick={() => setName(" Klik Nama Berubah")}>Klik Disini</button>
//       <p>{name}</p>
//     </div>
//   );
// }

// //--------------------------------------------------------------

// /**React Router
//  * install --> npm install react-router-dom
//  * import react-router-dom
//  */

// import { BrowserRouter, Routes, Route } from "react-router-dom";

// //ini adalah Komponentnya
// const Home = () => {
//   return (
//     <div>
//       <h1>Ini adalah Home</h1>
//     </div>
//   );
// };

// const About = () => {
//   return (
//     <div>
//       <h1>Ini adalah Halaman About</h1>
//     </div>
//   );
// };

// const Contact = () => {
//   return (
//     <div>
//       <h1>Ini adalah Halaman Contact</h1>
//     </div>
//   );
// };

// //Disini Render appnya
// function App() {
//   return (
//     <div>
//       <BrowserRouter>
//         <Routes>
//           <Route exact path="/" element={<Home />} />
//           <Route path="/about" element={<About />} />
//           <Route path="/contact" element={<Contact />} />
//         </Routes>
//       </BrowserRouter>
//     </div>
//   );
// }


//--------------------------------------------------------------

/**Fetch Data
 *
 */

import { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Productlist from "./components/Productlist";
import AddProduct from "./components/AddProduct";
import EditProduct from "./components/EditProduct";


function App() {
  return (
    <div className="container">
      <BrowserRouter>
        <Routes>
          <Route exact path="/" element={<Productlist />} />
          <Route path="/add" element={<AddProduct />} />
          <Route path="/edit/:id" element={<EditProduct />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}





export default App;
