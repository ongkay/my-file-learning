// getting-started.js
const mongoose = require('mongoose');
const { model, Schema } = mongoose;


// Cara connect ke mongodb :
const db = mongoose.connect

main().catch(err => console.log(err.message));

async function main() {
    await db('mongodb://localhost:27017/test') // nama db : test
    console.log("berhasil connect ke db")
}


// buat schema
const kelasSchema = new Schema({
  judul:  String, 
  deskripsi: String,
  tglPosting: { type: Date, default: Date.now }
});

//buat collection/Model nya
const Kelas = mongoose.model('Kelas', kelasSchema)

{//--CARA menyimpan data ke mongodb :

    {//Cara ke-1
        const nodejs = new Kelas({
            judul: 'nodejs',
            deskripsi: 'javascrript runtume built on chrome V8 javascript engine'
        }) 

        nodejs.save(function(error, sukses){
            if(error) console.log(error)

            console.log(sukses)
            console.log('sukses buat collection Kelas')
        })
    }

    {//*Cara ke-2
        const caradua = new Kelas() // instanse dari modelnya
        caradua.judul = "menggunakan cara ke-2"
        caradua.deskripsi = "ini menggunakan cara yang kedua gais dan menggunakan metode SAVE"

        caradua.save(function(error, sukses){
            if(error) console.log(error)

            console.log(sukses)
            console.log('sukses buat collection Kelas')
        })
    }

    {//Cara ketiga menggunakan Create 
        Kelas.create({
            judul: 'cara ketiga',
            deskripsi: 'ini menggunakan metode create bukan save'
        }, function(error, sukses){
            if(error) console.log(error)

            console.log(sukses)
            console.log('sukses buat collection Kelas')
        })
    }
}

{// -- query

    Kelas.find({deskripsi: /chrome/i}).select('judul').
    exec(function(error, sukses){
        if(error) console.log(error)

        console.log(sukses)
    })

    // find all documents
    await MyModel.find({});

    // find all documents named john and at least 18
    await MyModel.find({ name: 'john', age: { $gte: 18 } }).exec();

    // executes, passing results to callback
    MyModel.find({ name: 'john', age: { $gte: 18 }}, function (err, docs) {});

    // executes, name LIKE john and only selecting the "name" and "friends" fields
    await MyModel.find({ name: /john/i }, 'name friends').exec(); // hanya menampilkan field 'name' dan 'friends' aja

    // passing options
    await MyModel.find({ name: /john/i }, null, { skip: 10 }).exec();
}

{//--Query kedua
    // With a JSON doc
    Person.
      find({
        occupation: /host/,
        'name.last': 'Ghost',
        age: { $gt: 17, $lt: 66 },
        likes: { $in: ['vaporizing', 'talking'] }
      }).
      limit(10).
      sort({ occupation: -1 }).
      select({ name: 1, occupation: 1 }).
      exec(callback);

    // Using query builder
    Person.
      find({ occupation: /host/ }).
      where('name.last').equals('Ghost').
      where('age').gt(17).lt(66).
      where('likes').in(['vaporizing', 'talking']).
      limit(10).
      sort('-occupation').
      select('name occupation').
      exec(callback);
}
