{//--query
    // contoh cari _id :
    db.customers.find({
        _id: "khannedy"
    });

    // contoh cari name :
    db.customers.find({
        name: "Eko Kurniawan Khannedy"
    });

    // contoh cari harga :
    db.products.find({
        price: 2000
    });

    // akses embeded objek
    db.orders.find({
        "items.product_id": 1
    });

    // contoh cari name lebih dari satu :
    db.customers.find({
        _id: "khannedy",
        name: "Eko Kurniawan Khannedy"
    });
}

{//--query-comparison
    /**Contoh mencari id dengan nama khannedy di collection customers
     * ada 2 cara dibawah ini :
     * SQL : select * from customers where _id = 'khannedy'
     */
    db.customers.find({
        _id: {
            $eq: "khannedy"
        }
    });

    //atau cara simple nya :
    db.customers.find({
        _id: "khannedy"
    });


    /**contoh mencari harga yang harganya diatas 1000 di collection products
     * SQL : select * from products where price > 1000
     */
    db.products.find({
        price: {
            $gt: 1000
        }
    });



    // insert product documents
    db.products.insertMany([
        {
            _id: 3,
            name: "Pop Mie Rasa Bakso",
            price: new NumberLong(2500),
            category: "food"
        },
        {
            _id: 4,
            name: "Samsung Galaxy S9+",
            price: new NumberLong(10000000),
            category: "handphone"
        },
        {
            _id: 5,
            name: "Acer Precator XXI",
            price: new NumberLong(25000000),
            category: "laptop"
        }
    ]);

    /**Contoh cara mencari/filter produk di katagori "hanphone dan laptop" di collection products
     * dan dengan harga diatas 5000000
     * SQL : select * from products where category in ('handphone', 'laptop') and price > 5000000
     */
    db.products.find({
        category: {
            $in: ["handphone", "laptop"]
        },
        price: {
            $gt: 5000000
        }
    });
}

{//--query-logical

    {//Syntax Logical Operator

        //$and. $or, $nor
        db.collection.find({
            $operator : [
                {
                    // expression
                },
                {
                    //expression
                }
            ]
        })

        //$not
        db.collection.find({
            field: {
                $not: {
                    // operator expression
                }
            }
        })
    }

    {//contoh :
        /**contoh menampilkan semua produk yang ada di katagori "Laptop dan Hanphone dengan harga diatas 20000000" 
         * SQL : select * from products where category in ('laptop', 'handphone') and price > 20000000
        */
        db.products.find({
            $and: [
                {
                    category: {
                        $in: ["laptop", "handphone"]
                    }
                },
                {
                    price: {
                        $gt: 20000000
                    }
                }
            ]
        });

        /**ingin mencari/menampilkan produk yang katagorinya tidak didalam "laptop dan hanphone" 
         * SQL : select * from products where category not in ('laptop', 'handphone')
        */
        db.products.find({
            category: {
                $not: {
                    $in: ["laptop", "handphone"]
                }
            }
        });

        /**ingin mencari produk  yang harganya 1000 hingga 20000000 
         * dan katagorinya tidak sama dengan 'food'
         * SQL : select * from products where price between 10000000 and 20000000 and category != 'food'
        */
        db.products.find({
            $and: [
                {
                    price: {
                        $gte: 1000,
                        $lte: 20000000
                    }
                },
                {
                    category: {
                        $ne: 'food'
                    }
                }
            ]
        });
    }


}






