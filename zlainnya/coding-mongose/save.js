{
const express = require('express')
const router = express.Router()

let users = [
    {id: 1, name: 'budi', email: 'budi@gmail.com'},
    {id: 2, name: 'lelek', email: 'lelek@gmail.com'}
]


router.route('/users')
    .get((req, res) => {
        if(users.length > 0) {
            res.json({
                status: true,
                data: users,
                method: req.method,
                url: req.url
            })
        } else {
            res.json({
                status: false,
                message: 'Data user masih kosong gais'
            })
        }
    })

    .post((req, res) => {
        console.log(req.body) //akan tampil di terminal
        users.push(req.body)

        res.send({
            status: true,
            data: users,
            message: 'Data user berhasil di simpan',
            method: req.method,
            url: req.url
        })
        
    })


router.put('/users/:id', (req, res) => {
    const id = req.params.id
    users.filter(user => {
        if(user.id == id) {
            user.id = id
            user.name = req.body.name
            user.email = req.body.email

            return user
        }
    })

    // res.json(users)
    res.json({
        status: true,
        data: users,
        message: 'Data user berhasil di edit',
        method: req.method,
        url: req.url
    })
})

router.delete('/users/:userId', (req, res) => {
    const id = req.params.userId
    users = users.filter(user => user.id != id) // hapus user dengan filter
    
    // res.json(users)
    res.send({
        status: true,
        data: users,
        message: 'Data user berhasil di Hapus',
        method: req.method,
        url: req.url
    })
})



module.exports = router

// //cara panggilnya :
// app.use(userRouter)
}