const mongoose = require('mongoose');
const express = require('express')
const userRouter = require('./users')
const app = express()
const port = 3000
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

const myLogger = function (req, res, next) {
    req.time = new Date()
    next()
  }
app.use(myLogger) //aktivekan midleware

mongoose.connect('mongodb://localhost:27017/express'); //mongodb

app.set('view engine', 'ejs') //render views ejs
app.use(express.static('public'))

app.get('/', (req, res) => {
    const kelas = {
        id: 1,
        nama: "Javascript",
        date: req.time.toString()
    }
    res.render('pages/index', {kelas: kelas})
})


app.get('/about', (req, res) => {
    res.render('pages/about')
})

// //single route
// app.get('/users', (req, res) => {
//     res.send('Get User')
// })

// app.post('/users', (req, res) => {
//     res.send('Post User')
// })

// app.put('/users', (req, res) => {
//     res.send('Put User')
// })

// app.delete('/users', (req, res) => {
//     res.send('Delete User')
// })

// //Multi Route
// app.route('/users')
//     .get((req, res) => {
//         res.send('Get User')
//     })

//     .post((req, res) => {
//         res.send('Post User')
//     })

//     .put((req, res) => {
//         res.send('Put User')
//     })

//     .delete((req, res) => {
//         res.send('Delete User')
//     })

app.use(userRouter)


app.listen(port, () => {
  console.log(`Server is Okay`)
})


