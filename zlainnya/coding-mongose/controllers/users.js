const User = require("../models/user");

module.exports = {
  index: (req, res) => {
    let keyword = {};

    if (req.query.keyword) {
      keyword = {
        name: {
          $regex: req.query.keyword,
        },
      };
    }

    //Query cara 2
    const query = User.find(keyword);
    query.select("name _id");
    query.exec((error, sukses) => {
      if (error) console.log(error);

      console.log(sukses);
      res.render("pages/user/index", { users: sukses });
    });

    // //Query cara 1
    // User.find(keyword, "name _id", (error, sukses) => {
    //   if (error) console.log(error);

    //   console.log(sukses);
    //   res.render("pages/user/index", { users: sukses });
    // });
  },
  show: (req, res) => {
    const id = req.params.id;
    User.findById(id, (err, data) => {
      if (err) console.log(err);
      console.log(data);
      res.render("pages/user/show", { user: data });
    });
  },
  create: (req, res) => {
    res.render("pages/user/create");
  },
  store: (req, res) => {
    // //cara Pertama
    // const user = new User({
    //     name: req.body.name,
    //     email: req.body.email,
    //     password: req.body.password,
    // })
    // user.save((err, sukses) => {
    //     if(err) console.log(err)
    //     console.log(sukses)
    //     res.redirect('/users')
    // })

    //Cara kedua
    User.create(
      {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
      },
      (err, sukses) => {
        if (err) console.log(err);
        console.log(sukses);
        res.redirect("/users");
      }
    );

    // users.push({
    //     id: uuidv4(),
    //     name: req.body.name,
    //     email: req.body.email
    // })

    // console.log(users) //akan tampil di terminal
    // res.end() // biar di browsesr tidak menggantung
  },
  update: (req, res) => {
    const id = req.params.id;
    users.filter((user) => {
      if (user.id == id) {
        user.id = id;
        user.name = req.body.name;
        user.email = req.body.email;

        return user;
      }
    });

    res.json({
      status: true,
      data: users,
      message: "Data user berhasil di edit",
      method: req.method,
      url: req.url,
    });
  },
  delete: (req, res) => {
    const id = req.params.userId;
    users = users.filter((user) => user.id != id); // hapus user dengan filter

    // res.json(users)
    res.send({
      status: true,
      data: users,
      message: "Data user berhasil di Hapus",
      method: req.method,
      url: req.url,
    });
  },
};
