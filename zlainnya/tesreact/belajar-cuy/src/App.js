// import React, { useEffect, useState, useRef } from 'react';
import MainLayouts from './components/Layouts/main.layouts';
import Albums from './components/Albums/main.albums';

const App = () => {
  return (
    <>
      <MainLayouts>
        <Albums
          title="Image API"
          description="we fetch random image API from third party"
        />
      </MainLayouts>
    </>
  );
};

export default App;
