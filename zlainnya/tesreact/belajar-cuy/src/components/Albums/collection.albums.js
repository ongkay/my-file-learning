import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Carousel from 'react-bootstrap/Carousel';
import { ButtonGroup } from 'react-bootstrap';

const Collection = () => {
  const [datas, setDatas] = useState([]);
  const [limit, setLimit] = useState(3);

  useEffect(() => {
    let isCanceled = false;
    if (isCanceled === false) {
      axios({
        method: 'GET',
        url: `${process.env.REACT_APP_BASEURL}/photos?_limit=${limit}`,
      }).then((result) => setDatas(result.data));
    }
    return () => {
      isCanceled = true;
    };
  }, [limit]);

  const handleLimit = (options) => {
    options === '+'
      ? setLimit((prev) => prev + 1)
      : setLimit((prev) => prev - 1);
    // if (options === "+") setLimit((prev) => prev + 1);
    // if (options === "-") setLimit((prev) => prev - 1);
  };

  console.log(datas);

  return (
    <React.Fragment>
      <h3>{limit} Courosel</h3>
      <Carousel>
        {/* carousel items start */}
        {datas.map((data, i) => {
          return (
            <Carousel.Item key={i}>
              <img
                className="d-block w-100"
                src={data.url}
                alt="First slide"
                height={450}
                width={450}
              />
              <Carousel.Caption>
                <h3>{data.title}</h3>
              </Carousel.Caption>
            </Carousel.Item>
          );
        })}
        {/* carousel items end */}
      </Carousel>
      {/* <ButtonGroup>
        <button
          className="btn btn-outline-primary"
          onClick={() => handleLimit("+")}
        >
          +
        </button>
        {limit > 1 && (
          <button
            className="btn btn-outline-primary"
            onClick={() => handleLimit("-")}
          >
            -
          </button>
        )}
      </ButtonGroup> */}
    </React.Fragment>
  );
};

export default Collection;
