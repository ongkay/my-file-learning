import React, { useEffect, useState, useRef } from 'react';
import Testing from './components/testing';

const App = () => {
  const [getLimit, setLimit] = useState(1);
  const [getMyName, setMyName] = useState('saya');
  const inputNameRef = useRef(getMyName);
  useEffect(() => {
    console.log('getLimit: ', getLimit);
    setMyName(inputNameRef.current.value);
  }, [getLimit]);
  return (
    <>
      <center>
        {getMyName}
        {getLimit}
        <br />
        {/* <input placeholder="name" onChange={(e) => setMyName(e.target.value)} /> */}
        <input ref={inputNameRef} defaultValue={inputNameRef.current} />
        <button onClick={() => setLimit((a) => a + 1)}>Naikkan limit</button>
        <button onClick={() => setLimit((a) => a - 1)}>Turunkan limit</button>
      </center>
      <Testing title="ini judul" deskripsi="deskripsi" />
    </>
  );
};

export default App;
