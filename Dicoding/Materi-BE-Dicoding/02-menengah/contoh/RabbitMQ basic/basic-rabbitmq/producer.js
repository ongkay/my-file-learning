const amqp = require('amqplib')

const init = async () => {
  const connection = await amqp.connect('amqp://localhost') // [1]
  const channel = await connection.createChannel() // [2]

  // [3]
  const queue = 'dicoding'
  const message = 'Selamat belajar message broker!'

  // [4]
  await channel.assertQueue(queue, {
    durable: true,
  })

  // [5]
  await channel.sendToQueue(queue, Buffer.from(message))
  console.log('Pesan berhasil terkirim!')

  // [6]
  setTimeout(() => {
    connection.close()
  }, 1000)
}

init()

/**NOTR:
 * [1] buat koneksi terlebih dahulu pada server RabbitMQ yang sudah dipasang di komputer kita
 * [2] uat objek channel yang digunakan untuk memanggil API dalam mengoperasikan transaksi di protokol AMQP
 * [3] untuk mengirimkan pesan, kita perlu menentukan dahulu nama dari queue yang dituju dan isi dari pesannya. Kita buat variable queue dengan nilai ‘dicoding’ dan message dengan nilai ‘Selamat belajar message broker!’.
 * [4] Sebelum mengirimkan pesan ke queue, kita perlu memastikan dulu bahwa queue dengan nama dicoding sudah dibuat. Caranya dengan menggunakan fungsi channel.assertQueue.
 * - Fungsi tersebut menerima dua parameter: pertama adalah nama queue yang akan diperiksa dan kedua adalah objek options.
 * - channel.assertQueue bersifat idempoten, yang berarti ia hanya akan membuat channel baru bila channel yang diperiksa tidak ada. Properti durable pada options berfungsi untuk menjaga agar queue tetap tersedia ketika server message broker restart.
 * [5] Setelah memastikan queue tersedia, barulah kita bisa mengirimkan pesan dengan perintah channel.sendToQueue.
 * - Fungsi sendToQueue menerima dua parameter, yaitu nama queue dan pesan dalam bentuk Buffer. Maka dari itu, kita perlu mengubah pesan menjadi bentuk buffer melalui perintah Buffer.from.
 * - Setelah fungsi sendToQueue dijalankan, seharusnya pesan sudah terkirim ke RabbitMQ server.
 * [6] Best practice ketika selesai mengirimkan pesan ke broker adalah tutup koneksi yang sebelumnya dibuat. Beri jeda minimal 1 detik setelah pengiriman pesan guna menghindari celah waktu dalam pengiriman.
 * [GO] Buka browser; kunjungi halaman http://localhost:15672; dan login sebagai guest.
 */
