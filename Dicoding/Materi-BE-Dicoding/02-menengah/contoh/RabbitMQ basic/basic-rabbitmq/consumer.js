const amqp = require('amqplib')

const init = async () => {
  const connection = await amqp.connect('amqp://localhost')
  const channel = await connection.createChannel()

  const queue = 'dicoding'

  await channel.assertQueue(queue, {
    durable: true,
  })

  channel.consume(
    // [1]
    queue,
    // [2]
    (message) => {
      console.log(`Menerima pesan dari queue ${queue}: ${message.content.toString()}`)
    },
    // [3]
    { noAck: true },
  )
}

init()

/**Fungsi consume menerima tiga parameter, berikut penjelasan singkatnya:
 * [1] Queue :
 * - Nama dari queue yang dikonsumsi. Consumer akan selalu mengamati untuk menerima pesan masuk yang dikirimkan ke queue tersebut.
 * [2] Callback function :
 * - ungsi yang akan dijalankan ketika ada pesan masuk ke dalam queue yang diamati. Fungsi ini membawa pesan berupa instance dari amqp.ConsumeMessage yang berisikan konten yang dikirim oleh producer. Kita dapat membaca kontennya dalam bentuk string dengan menggunakan fungsi message.content.toString()
 * [3] Options:
 * - Objek yang menentukan seperti apa perilaku ketika pesan diterima. Salah satu properti objeknya adalah noAck, yang menunjukkan apakah penerimaan pesan butuh pengakuan (acknowledgement) atau tidak.
 *  node consumer.js
 */
