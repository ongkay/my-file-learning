{//--notes.js
const notes = [];
 
module.exports = notes;
}


{//--server.js
    const Hapi = require('@hapi/hapi');
    const routes = require('./routes');
    
    const init = async () => {
    const server = Hapi.server({
        port: 5000,
        host: 'localhost',
        routes: {
        cors: {
            origin: ['*'],
        },
        },
    });
    
    server.route(routes);
    
    await server.start();
    console.log(`Server berjalan pada ${server.info.uri}`);
    };
    
    init();
}


{//--routes.js
    const {
        addNoteHandler,
        getAllNotesHandler,
        getNoteByIdHandler,
        editNoteByIdHandler,
        deleteNoteByIdHandler,
    } = require('./handler');
    
    const routes = [
        {
            method: 'POST',
            path: '/notes',
            handler: addNoteHandler,
        },
        {
            method: 'GET',
            path: '/notes',
            handler: getAllNotesHandler,
        },
        {
            method: 'GET',
            path: '/notes/{id}',
            handler: getNoteByIdHandler,
        },
        {//Merubah catatan
            method: 'PUT',
            path: '/notes/{id}', 
            handler: editNoteByIdHandler,
        },
        {
            method: 'DELETE',
            path: '/notes/{id}',
            handler: deleteNoteByIdHandler,
        },
    ];
        
        module.exports = routes;
}


{//--handler.js
    const notes = require('./notes');
    const { nanoid } = require('nanoid');
    
    //menambahkan catatan baru
    const addNoteHandler = (request, h) => {
        const { title, tags, body } = request.payload;
    
        const id = nanoid(16);
        const createdAt = new Date().toISOString();
        const updatedAt = createdAt;
    
        const newNote = {
        title, tags, body, id, createdAt, updatedAt,
        };
    
        notes.push(newNote);
    
        const isSuccess = notes.filter((note) => note.id === id).length > 0;
    
        if (isSuccess) {
        const response = h.response({
            status: 'success',
            message: 'Catatan berhasil ditambahkan',
            data: {
            noteId: id,
            },
        });
        response.code(201);
        return response;
        }

        const response = h.response({
        status: 'fail',
        message: 'Catatan gagal ditambahkan',
        });

        response.code(500);
        return response;
    };


    const getAllNotesHandler = () => ({
        status: 'success',
        data: {
            notes,
        },
    });
    
    //Menampilkan catatan
    const getNoteByIdHandler = (request, h) => {
        const { id } = request.params;
    
        const note = notes.filter((n) => n.id === id)[0];
    
    if (note !== undefined) {
        return {
            status: 'success',
            data: {
            note,
            },
        };
        }
    
        const response = h.response({
        status: 'fail',
        message: 'Catatan tidak ditemukan',
        });
        response.code(404);
        return response;
    };
    

    //edit atau merubah catatan
    const editNoteByIdHandler = (request, h) => {
    const { id } = request.params;
    
    const { title, tags, body } = request.payload;
    const updatedAt = new Date().toISOString();
    
    const index = notes.findIndex((note) => note.id === id);
    
    if (index !== -1) {
        notes[index] = {
        ...notes[index],
        title,
        tags,
        body,
        updatedAt,
        };
    
        const response = h.response({
        status: 'success',
        message: 'Catatan berhasil diperbarui',
        });
        response.code(200);
        return response;
    }
    
    const response = h.response({
        status: 'fail',
        message: 'Gagal memperbarui catatan. Id tidak ditemukan',
    });
    response.code(404);
    return response;
    };
    

    //hapus catatan
    const deleteNoteByIdHandler = (request, h) => {
    const { id } = request.params;
    
    const index = notes.findIndex((note) => note.id === id);
    
    if (index !== -1) {
        notes.splice(index, 1);
        const response = h.response({
        status: 'success',
        message: 'Catatan berhasil dihapus',
        });
        response.code(200);
        return response;
    }
    
    const response = h.response({
        status: 'fail',
        message: 'Catatan gagal dihapus. Id tidak ditemukan',
    });
    response.code(404);
    return response;
    };
    
    module.exports = {
    addNoteHandler,
    getAllNotesHandler,
    getNoteByIdHandler,
    editNoteByIdHandler,
    deleteNoteByIdHandler,
    };
}

