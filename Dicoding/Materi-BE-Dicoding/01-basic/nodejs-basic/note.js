{//--Events

{//Contoh :
    const { EventEmitter } = require('events');
    
    const myEventEmitter = new EventEmitter();


    {//Cara pertama :

        // fungsi yang akan dijalankan ketika event coffee-order terjadi
        const makeCoffee = ({ name }) => {
            console.log(`Kopi ${name} telah dibuat!`);
        };
        
        const makeBill = ({ price }) => {
            console.log(`Bill sebesar ${price} telah dibuat!`);
        }
        

        // mendaftarkan fungsi makeCoffee sebagai listener event coffee-order
        myEventEmitter.on('coffee-order', makeCoffee);
        myEventEmitter.on('coffee-order', makeBill);


        // Memicu atau membangkitkan event 'coffee-order' terjadi.
        myEventEmitter.emit('coffee-order', { name: 'Tubruk', price: 15000 });
        

        /**
         * output:
         * Kopi Tubruk telah dibuat!
         * Bill sebesar 15000 telah dibuat!
         */
    }

    {// Cara ke-2 : 
    /**Membuat satu fungsi khusus untuk menangani event. 
     * Biasanya fungsi ini memiliki nama ‘handler’ atau ‘listener’ pada akhir penamaanya
     */

        // fungsi yang akan dijalankan ketika event coffee-order terjadi
        const makeCoffee = (name) => {
            console.log(`Kopi ${name} telah dibuat!`);
        };
        
        const makeBill = (price) => {
            console.log(`Bill sebesar ${price} telah dibuat!`);
        }
        
        //fungsi khusus untuk menangani event, inilah perbedaannya dari cara pertama diatas.
        const onCoffeeOrderedListener = ({ name, price }) => {
            makeCoffee(name);
            makeBill(price);
        }
        
    // mendaftarkan fungsi makeCoffee sebagai listener event coffee-order
        myEventEmitter.on('coffee-order', onCoffeeOrderedListener);
        
        // Memicu atau membangkitkan event 'coffee-order' terjadi.
        myEventEmitter.emit('coffee-order', { name: 'Tubrukk', price: 15000 });
        
        /**
         * output:
         * Kopi Tubruk telah dibuat!
         * Bill sebesar 15000 telah dibuat!
         */
    }
}

{//Latihannya :
    // TODO 1
    //Buat atau impor variabel EventEmitter dari core module events
    const { EventEmitter } = require('events');

    const birthdayEventListener = (name) => {
        console.log(`Happy birthday ${name}!`);
    }

    // TODO 2
    //variabel myEmitter yang merupakan instance dari EventEmitter
    const myEmitter = new EventEmitter();
    
    // TODO 3
    //birthdayEventListener sebagai aksi ketika event ‘birthday’ dibangkitkan pada myEmitter
    myEmitter.on('birthday', birthdayEventListener);

    
    // TODO 4
    //Bangkitkanlah event ‘birthday’ pada myEmitter dengan method emit() dan beri nilai argumen listener dengan nama Anda
    myEmitter.emit('birthday', "ongki");
}

}


{//--fs
    {
    /**Node.js menyediakan core modules fs yang dapat mempermudah kita dalam mengakses filesystem. 
     * Setiap method yang ada di module fs tersedia dalam dua versi, 
     * yakni versi asynchronous (default) dan versi synchronous. 
     */
     const fs = require('fs');
    
     //versi Asyncronous :
     /**fs.readFile() menerima tiga argumen yakni: lokasi berkas, encoding, 
      * dan callback function yang akan terpanggil bila berkas berhasil/gagal diakses. 
      */
 
     const fileReadCallback = (error, data) => {
         if(error) {
             console.log('Gagal membaca berkas');
             return;
         }
         console.log(data);
     };
     
     fs.readFile('todo.txt', 'UTF-8', fileReadCallback);
 
 
 
     //versi Syncronous :
     const data = fs.readFileSync('todo.txt', 'UTF-8');
     console.log(data);
    }

    {//--fs readableStream & writableStream

        const fs = require('fs');
        
        //readableStream
            const readableStream = fs.createReadStream('./article.txt', {
                highWaterMark: 10 //==> ukuran buffer 10 mb
            });
            
            readableStream.on('readable', () => {
                try {
                    process.stdout.write(`[${readableStream.read()}]`); //==> akan di tampilkan di log
                } catch(error) {
                }
            });
            
            readableStream.on('end', () => {
                console.log('Done');
            });
        
        
        
        //writableStream
            //Akan membuat folder baru atau menimpa isi output.txt
            const writableStream = fs.createWriteStream('output.txt');
            writableStream.write('Ini merupakan teks baris pertama!\n');
            writableStream.write('Ini merupakan teks baris kedua!\n');
            writableStream.end('Akhir dari writable stream!'); //==> diakhiri dengan .end
        
        
        {//Contoh Latihan 
        /**
         * TODO:
         * Buatlah program untuk membaca teks input.txt dan menuliskannya ulang pada berkas output.txt
         * menggunakan teknik readable stream dan writable stream.
         */
            const fs = require('fs');
            const path = require('path')
            
            const lokasiOutput = path.resolve(__dirname, 'output.txt');
            const lokasiInput = path.resolve(__dirname, 'input.txt');
        
        
            const readableStream = fs.createReadStream(lokasiInput, {
                highWaterMark: 15 
            });
            
            const writableStream = fs.createWriteStream(lokasiOutput);
        
        
            readableStream.on('readable', () => {
                try {
                //    process.stdout.write(`[${readableStream.read()}]`);
                    writableStream.write(`${readableStream.read()}\n`); //==> akan menulis hasil input ke output
        
                } catch(error) {
                // console.log(error)
                }
            });
            
            readableStream.on('end', () => {
                console.log('Done');
                writableStream.end(); //==> jgn lupa di akhiri dengan .end
            });
        }
    
    }
    
 }
