{//--fs readableStream & writableStream

    const fs = require('fs');
    
//readableStream
    const readableStream = fs.createReadStream('./article.txt', {
        highWaterMark: 10 //==> ukuran buffer 10 mb
    });
    
    readableStream.on('readable', () => {
        try {
            process.stdout.write(`[${readableStream.read()}]`); //==> akan di tampilkan di log
        } catch(error) {
        }
    });
    
    readableStream.on('end', () => {
        console.log('Done');
    });



//writableStream
    //Akan membuat folder baru atau menimpa isi output.txt
    const writableStream = fs.createWriteStream('output.txt');
    writableStream.write('Ini merupakan teks baris pertama!\n');
    writableStream.write('Ini merupakan teks baris kedua!\n');
    writableStream.end('Akhir dari writable stream!'); //==> diakhiri dengan .end


{//Contoh Latihan 
/**
 * TODO:
 * Buatlah program untuk membaca teks input.txt dan menuliskannya ulang pada berkas output.txt
 * menggunakan teknik readable stream dan writable stream.
 */
    const fs = require('fs');
    const path = require('path')
    
    const lokasiOutput = path.resolve(__dirname, 'output.txt');
    const lokasiInput = path.resolve(__dirname, 'input.txt');
   
   
    const readableStream = fs.createReadStream(lokasiInput, {
        highWaterMark: 15 
    });
     
    const writableStream = fs.createWriteStream(lokasiOutput);
   
   
    readableStream.on('readable', () => {
        try {
        //    process.stdout.write(`[${readableStream.read()}]`);
            writableStream.write(`${readableStream.read()}\n`); //==> akan menulis hasil input ke output
   
        } catch(error) {
           // console.log(error)
        }
    });
     
    readableStream.on('end', () => {
        console.log('Done');
        writableStream.end(); //==> jgn lupa di akhiri dengan .end
    });
}

}

