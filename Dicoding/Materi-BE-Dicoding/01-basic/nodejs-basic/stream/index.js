 /**
 * TODO:
 * Buatlah program untuk membaca teks input.txt dan menuliskannya ulang pada berkas output.txt
 * menggunakan teknik readable stream dan writable stream.
 */
 const fs = require('fs');
 const path = require('path')
 
 const lokasiOutput = path.resolve(__dirname, 'output.txt');
 const lokasiInput = path.resolve(__dirname, 'input.txt');


 const readableStream = fs.createReadStream(lokasiInput, {
     highWaterMark: 15 
 });
  
 const writableStream = fs.createWriteStream(lokasiOutput);


 readableStream.on('readable', () => {
     try {
        process.stdout.write(`[${readableStream.read()}]`);
         writableStream.write(`${readableStream.read()}\n`);

     } catch(error) {
        // console.log(error)
     }
 });
  
 readableStream.on('end', () => {
     console.log('Done');
     writableStream.end();
 });
 
 





