"use strict";
{
    let nama;
    nama = 'saya';
    let umur;
    umur = 19;
    let isMarried;
    isMarried = true;
    isMarried = false;
    let heroes;
    heroes = "mantap";
    heroes = 123;
    heroes = ["mantap"];
    let mytipe;
    mytipe = 'hello';
    let multi;
    multi = 'hello';
    multi = 123;
    let isArray;
    isArray = [];
    let arrayOfNumber;
    arrayOfNumber = [1, 2, 3];
    let arrayOfString;
    arrayOfString = ['mantap', "super", "saya"];
    let arrayOfAny;
    arrayOfAny = ['mantap', 123, true];
    let biodata;
    biodata = ['surabaya', 123];
    let dataUser = {
        name: "Saya",
        age: 20
    };
    let Month;
    (function (Month) {
        Month["JAN"] = "Januari";
        Month["FEB"] = "Februari";
        Month["MAR"] = "Maret";
        Month["APR"] = "April";
    })(Month || (Month = {}));
    console.log(Month.APR);
}
{
    function getName() {
        return "fungtion ini kembalian string";
    }
    console.log(getName());
    function printName() {
        console.log(getName());
    }
    printName();
    function sum(a, b) {
        return a + b;
    }
    console.log(sum(2, 4));
    let g;
    g = function () {
        return null;
    };
    const add = (val1, val2) => {
        return val1 + val2;
    };
    console.log(add(100, 500));
    const fullname = (first, last = "Sutarno", adress) => {
        return first + " " + last;
    };
    console.log(fullname("Sijar"));
    console.log(fullname('Sijar', 'moko'));
}
{
    class User1 {
        constructor(name) {
            this.name = name;
        }
    }
    const result1 = new User1('saya');
    console.log(result1.name);
    class User2 {
        constructor(name) {
            this.name = name;
        }
    }
    const result2 = new User2('saya');
    console.log(result2.name);
    {
        class Namaku {
            constructor(theName) {
                this.pekerjaan = 'programmer';
                this.nama = theName;
            }
        }
        let namaku = new Namaku("Andri");
        namaku.pekerjaan = "bisnismen";
    }
}
class Users {
    constructor(name, age) {
        this.getName = () => { return this.name; };
        this.name = name;
        this.age = age;
    }
    setName(value) {
        this.name = value;
    }
}
class Admin extends Users {
    constructor() {
        super(...arguments);
        this.read = true;
        this.write = true;
    }
    getRole() {
        return {
            read: this.read,
            write: this.write
        };
    }
}
let admin = new Admin('toni', 25);
admin.getName();
admin.getRole();
admin.setName('siapa');
class Admin2 extends Users {
    constructor(name, age, phone) {
        super(name, age);
        this.read = true;
        this.write = true;
        this.phone = phone;
    }
    getRole() {
        return {
            read: this.read,
            write: this.write
        };
    }
}
let admin2 = new Admin2('toni', 25, "081995970069");
admin2.getName();
admin2.getRole();
admin2.setName('siapa');
class Admin3 extends Users {
    constructor(name, age, phone) {
        super(name, age);
        this.read = true;
        this.write = true;
        this._email = "";
        this.phone = phone;
    }
    set email(value) {
        if (value.length < 5) {
            this._email = "email yang anda masukkan salah";
        }
        else {
            this._email = value;
        }
    }
    get email() {
        return this._email;
    }
    getRole() {
        return {
            read: this.read,
            write: this.write
        };
    }
}
let admin3 = new Admin3('toni', 25, '081995970069');
admin3.getName();
admin3.getRole();
admin3.setName('siapa');
admin3.email = "iniemailnya@gmail.com";
console.log(admin3.email);
let Admin4 = (() => {
    class Admin4 extends Users {
        constructor(name, age, phone) {
            super(name, age);
            this.read = true;
            this.write = true;
            this._email = "";
            this.phone = phone;
        }
        static getNameRole() {
            return "hai";
        }
        set email(value) {
            if (value.length < 5) {
                this._email = "email yang anda masukkan salah";
            }
            else {
                this._email = value;
            }
        }
        get email() {
            return this._email;
        }
        getRole() {
            return {
                read: this.read,
                write: this.write
            };
        }
    }
    Admin4.getRoleName = 'Admin';
    return Admin4;
})();
let admin4 = Admin4.getNameRole();
console.log(admin4);
class Kendaraan {
    start() {
        console.log("bruummmm");
    }
}
class Mobil extends Kendaraan {
    constructor() {
        super(...arguments);
        this.roda = 4;
        this.spion = 2;
    }
}
class Motor extends Kendaraan {
    constructor() {
        super(...arguments);
        this.roda = 2;
    }
}
let mobil = new Mobil();
console.log(mobil.roda);
console.log(mobil.spion);
mobil.start();
let motor = new Motor();
console.log(motor.roda);
motor.start();
class Asus {
    constructor(name, isGaming) {
        this.name = name;
        this.isGaming = isGaming;
    }
    on() {
        console.log("nyala");
    }
    off() {
        console.log("mati");
    }
}
let asus = new Asus('Asus', true);
console.log(asus.on());
console.log(asus.off());
class MacBook {
    constructor(name, keyboardLight) {
        this.name = name;
        this.keyboardLight = keyboardLight;
    }
    on() {
        console.log("nyala");
    }
    off() {
        console.log("mati");
    }
}
let macBook = new MacBook('MBP', true);
console.log(macBook.on());
console.log(macBook.off());
function getDataAny(value) {
    return value;
}
function getData(value) {
    return value;
}
const getDataArrow = (value) => {
    return value;
};
console.log(getDataAny("dataku"));
console.log(getData("dataku"));
console.log(getDataArrow("dataku"));
class List {
    constructor(...elements) {
        this.data = elements;
    }
    add(elements) {
        this.data.push(elements);
    }
    addMultiple(...elements) {
        this.data.push(...elements);
    }
    getAll() {
        return this.data;
    }
}
let random = new List(1, "b", 'c', 5);
random.add('sdkjfks');
random.add('kedua');
random.addMultiple(123, 124);
console.log(random.getAll());
