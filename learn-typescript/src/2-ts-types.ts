// type inference(Implicit)
let ti = 'hello';

// type annotation(Explicit)
let ta = 'hello tss';


let a; // undefined

let b: boolean;
b = true; // boolean

let c: number
c = 1; // number

let d: string
d = 'hello' // string

let e: bigint;
e = 100n; // big int (starting ES2020)

let f: symbol;
f = Symbol('Sym'); // symbol  (starting ES2015)

let h: null;
h = null; // null ( special primitive )

let i: {}
i = {}; // Object Literal

let j: []
j = []; // array







































